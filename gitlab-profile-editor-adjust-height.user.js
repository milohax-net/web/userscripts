// ==UserScript==
// @name         GitLab Profile Editor - Adjust Source Editor Height
// @namespace    http://milohax.net/
// @license      CC-BY-NC-SA-4.0
// @version      0.2
// @description  Make the source editor element on GitLab profile editor use most of the window space
// @author       mjl, duo
// @match        https://gitlab.com/*/-/ci/editor
// @icon         https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png
// @grant        none
// @downloadURL  https://gitlab.com/milohax-net/web/userscripts/-/raw/main/gitlab-profile-editor-adjust-height.user.js
// @updateURL    https://gitlab.com/milohax-net/web/userscripts/-/raw/main/gitlab-profile-editor-adjust-height.user.js
// ==/UserScript==

(function() {
  'use strict';

  function adjustEditorHeight() {
      const selectors = [
          '.top-bar-container',
          '.gl-card-header',
          '.gl-card-body',
          '.gl-tabs-nav',
          '.commit-group',
          'div.gl-transition-all:nth-child(2) > div:nth-child(3) > form:nth-child(1)',
          '.gl-py-5'
      ];

      let totalHeight = 0;
      selectors.forEach(selector => {
          const element = document.querySelector(selector);
          if (element) {
              totalHeight += element.offsetHeight;
          }
      });

      const fudge = 100; // Add some fudging to prevent scrollbar
      const newEditorHeight = (window.innerHeight - totalHeight) - fudge;

      const styleElement = document.createElement('style');
      styleElement.textContent = `
          [id^=source-editor-] {
              height: ${newEditorHeight}px !important;
          }
      `;
      document.head.appendChild(styleElement);
  }

    // Run the function after loading the page
//    window.addEventListener('load', adjustEditorHeight);

    // Run the function when the window is resized
    window.addEventListener('resize', adjustEditorHeight);

    // Use MutationObserver to detect changes in the DOM
    const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
            if (mutation.addedNodes && mutation.addedNodes.length > 0) {
                adjustEditorHeight();
            }
        });
    });

    observer.observe(document.body, {
        childList: true,
        subtree: true
    });

})();

