# Userscripts for web page augmentation.

## Description
My personal [userscripts](https://en.wikipedia.org/wiki/Userscript)↗ with modifications/opinions.

Most of these will be small changes to scripts sourced from one of these online Userscript repositories:

- [Greasemonkey](https://www.greasespot.net/)↗
- [Greasyfork](https://greasyfork.org)↗
- [Tampermonkey](https://www.tampermonkey.net/)↗

## Installation
Install a userscript manager extension for your browser. I am using [Violentmonkey](https://violentmonkey.github.io/) in 2024, which has extensions for Firefox, Edge, and Chrome.

## Usage
The scripts can be installed by visiting the "raw" version of the files. The userscript manager extension should associate `.user.js` files as being Userscripts and open them in the manager's library to import or update.

## Hacking

[Good explanation and how-to for userscripts](https://www.youtube.com/watch?v=U4dSWJFIQ0A)📺↗

Note that while Firefox doesn't have "snippets" like Chrome, you can save the console session with <kbd>⌘</kbd>-<kbd>S</kbd> and load from a file with <kbd>⌘</kbd>-<kbd>O</kbd>, and you get completion in the console (and externally in VSCode). So you're not missing anything, and the dev tools are better.

The [Violentmonkey](https://violentmonkey.github.io/)↗ userscript manager has a nice dark interface and seems to work as well as Tampermonkey/Greasemonkey and others. It's what we use at work for enhancing some work tools.

Good resources for learning about Userscripts

- [Sample userscript](https://gist.github.com/nikhilkumarsingh/11de9d7cb1221545a61c9a0294e7c7b7)↗
- [Useful links](https://gist.github.com/nikhilkumarsingh/6c3756baad82d8ad70e3fda3a493e651)↗
- [userscript-collection (GitHub)](https://github.com/topics/userscript-collection)↗
- [Awesome-userscripts (listings and links to tutorials)](https://github.com/awesome-scripts/awesome-userscripts)↗
