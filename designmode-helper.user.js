// ==UserScript==
// @name         DesignMode Helper
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Enables designMode editing without web app shortcuts interference
// @author       Mike Lockhart
// @match        *://*/*
// @grant        none
// @downloadURL  https://gitlab.com/milohax-net/web/userscripts/-/raw/main/designmode-helper.user.js
// @updateURL    https://gitlab.com/milohax-net/web/userscripts/-/raw/main/designmode-helper.user.js
// ==/UserScript==

(function() {
    'use strict';
    const blockShortcuts = function(e) {
        if (document.designMode === 'on') {
            e.stopImmediatePropagation();
        }
    };
    document.addEventListener('keydown', blockShortcuts, true);
    document.addEventListener('keypress', blockShortcuts, true);
    document.designMode = 'on';
})();