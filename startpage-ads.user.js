// ==UserScript==
// @name         Remove ads from Startpage search result.
// @license      GPL-3.0-or-later
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Remove ads.
// @author       ntq, mjl
// @match        https://www.startpage.com/*
// @icon         https://www.startpage.com/sp/cdn/favicons/favicon-196x196--default.png
// @grant        none
// @downloadURL  https://gitlab.com/milohax-net/web/userscripts/-/raw/main/startpage-ads.user.js
// @updateURL    https://gitlab.com/milohax-net/web/userscripts/-/raw/main/startpage-ads.user.js
// ==/UserScript==

///// Original https://update.greasyfork.org/scripts/458425/Remove%20ads%20from%20Startpage%20search%20result.user.js

function removeAds() {
  'use strict';

  let iframes = Array.from(document.getElementsByTagName("iframe"));

  while (iframes.length > 0) {
    iframes.pop().remove();
    console.info("Removed Ad")
   }
}

removeAds();

// In some cases the iframes will load later, so also observe mutations and remove
//   https://stackoverflow.com/questions/2844565/is-there-a-javascript-jquery-dom-change-listener

var observer = new window.MutationObserver(function(mutations, observer) {
  // fired when a mutation occurs
  console.info("Mutation observed", mutations, observer);
  removeAds();
});

// define what element should be observed by the observer
// and what types of mutations trigger the callback
observer.observe(document, {
  subtree: true,
  childList: true
});
