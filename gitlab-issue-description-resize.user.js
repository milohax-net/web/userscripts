// ==UserScript==
// @name         GitLab Adjust Markdown Area Height
// @namespace    http://milohax.net/
// @license      CC-BY-NC-SA-4.0
// @version      0.5
// @description  Removes the maxHeight restriction on markdownAreas in GitLab UI pages
// @author       mjl, duo
// @match        https://*.gitlab.com/*
// @icon         https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png
// @grant        none
// @downloadURL  https://gitlab.com/milohax-net/web/userscripts/-/raw/main/gitlab-issue-description-resize.user.js
// @updateURL    https://gitlab.com/milohax-net/web/userscripts/-/raw/main/gitlab-issue-description-resize.user.js
// ==/UserScript==

(function() {
    'use strict';

    function adjustHeight() {
        const markdownAreas = document.querySelectorAll('.markdown-area');
        const screenHeight = window.innerHeight;
        const percentageHeight = 50; // Adjust this value to change the percentage

        markdownAreas.forEach(area => {
            area.style.maxHeight = 'none';
            area.style.height = `${screenHeight * (percentageHeight / 100)}px`;
            area.style.resize = 'vertical'; // Allow vertical resizing
        });
    }

    // Run the function after loading the page
    window.addEventListener('load', adjustHeight);

    // Rerun the function when the window is resized
    window.addEventListener('resize', adjustHeight);

    // Create a MutationObserver to watch for dynamically added elements
    const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
            if (mutation.addedNodes && mutation.addedNodes.length > 0) {
                adjustHeight();
            }
        });
    });

    // Start observing the document with the configured parameters
    observer.observe(document.body, { childList: true, subtree: false });
})();